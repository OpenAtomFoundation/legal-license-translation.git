# 源译识 | 征集开源许可证中文译文，欢迎大家译起来！

- *请您注意，本仓库已搬迁到[新仓库](https://atomgit.com/translation/license-translation)，本仓库已停止维护。*
- *Please kindly notice that, this repo is no longer active, and is replaced and superseded by the [new repo](https://atomgit.com/translation/license-translation).*


“源译识/Contransus"项目是由开放原子开源基金会发起的开源公益翻译项目，聚焦于开源许可证、司法案例、专业书籍、重点报告及社区资讯等开源相关内容的专业化、社区化高质量翻译，旨在通过共译凝聚对开源的共识，促进开源领域不同语境和法域之间的交流、理解，从而反哺开源生态。欢迎您通过专项投稿、社区讨论、内容推荐、外部推荐等方式共同参与本项目。如您对本项目有任何意见或建议，请邮件联系translation@openatom.io。详情请见：https://atomgit.com/translation . 当前仓库为源译识项目旗下**开源许可证翻译板块**。

在此，我们非常感谢 **Lotus、[Vanessa](http://gitee.com/vanessaguo)、[Peaksol](http://gitee.com/peaksol)、[野行僧郭晧](http://gitee.com/gzkoala)、[迷糊](http://gitee.com/xriqc)、[赵振华](http://gitee.com/richzhao409)、Sikkim、[Sara](http://gitee.com/zhang-glas)、[OliviaCat](https://gitee.com/oliviacat)、[欧轩琦](https://gitee.com/ouxuanqi)、[KUMA](https://gitee.com/xue-yangjie)、[Leo](https://gitee.com/jin-sihan777)、[Fenju](https://gitee.com/fenju)** (按首次投稿顺序排序)13位译者和首届译文评审组 **周明辉教授、卫sir、沈芬律师、徐美玲助理教授、宋雷教授** 的贡献！