> This is an unofficial translation of the GNU Lesser General Public License into Simplified Chinese, contributed by OpenAtom Foundation. It was not published by the Free Software Foundation, and does not legally state the distribution terms for software that uses the GNU LGPL—only the original English text of the GNU GPL does that. However, we hope that this translation will help Simplified Chinese speakers understand the GNU LGPL better. You may publish this translation, modified or unmodified, only under the terms at https://www.gnu.org/licenses/translations.html.

> 这是GNU较宽松通用公共许可证（GNU LGPL）的非正式简体中文翻译，由开放原子开源基金会组织翻译。它并未被自由软件基金会发布，也并非使用GNU LGPL软件的分发条款的法律声明——只有GNU LGPL的原始英文版才有法律意义。不过，我们希望本翻译能够帮助简体中文用户更好地理解GNU LGPL。您可以发布本翻译的修改版或原版，但您必须基于 http://www.gnu.org/licenses/translations.html 的条款发布。

## GNU LESSER GENERAL PUBLIC LICENSE
## GNU较宽松通用公共许可证
## Version 3, 29 June 2007
## 第三版，2007年6月29日

```
Copyright © 2007 Free Software Foundation, Inc. <https://fsf.org/>
Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.
This version of the GNU Lesser General Public License incorporates the terms and conditions of version 3 of the GNU General Public License, supplemented by the additional permissions listed below.

版权所有 © 2007 自由软件基金会公司  <https://fsf.org/>
任何人皆可复制或分发本许可证的完整副本，但不得修改。
本版本的GNU 较宽松通用公共许可证包含了 GNU 通用公共许可证第 3 版的条款和条件，并另增下列附加许可 。

```


**0. Additional Definitions. 附加定义**

As used herein, “this License” refers to version 3 of the GNU Lesser General Public License, and the “GNU GPL” refers to version 3 of the GNU General Public License.

就本文而言，“本许可证”是指 GNU 较宽松通用公共许可证第3版，“GNU GPL ”是指 GNU 通用公共许可证第 3 版。

“The Library” refers to a covered work governed by this License, other than an Application or a Combined Work as defined below.

“本库”是指受本许可证约束的被覆盖作品，但不包括下文定义的应用程序或组合作品。

An “Application” is any work that makes use of an interface provided by the Library, but which is not otherwise based on the Library. Defining a subclass of a class defined by the Library is deemed a mode of using an interface provided by the Library.

“应用程序”是指使用本库所提供的接口但不基于库的任何作品。在本库定义的类下定义一个子类，被视为是一种调用了本库接口的方式。

A “Combined Work” is a work produced by combining or linking an Application with the Library. The particular version of the Library with which the Combined Work was made is also called the “Linked Version”.

“组合作品”指将应用程序和本库组合或链接在一起而产生的作品。制作组合作品的本库的特定版本被称为“被链接版本”。   

The “Minimal Corresponding Source” for a Combined Work means the Corresponding Source for the Combined Work, excluding any source code for portions of the Combined Work that, considered in isolation, are based on the Application, and not on the Linked Version.

组合作品的“最小对应源代码”是指组合作品的对应源代码，不包括在组合作品中被单独考虑为基于应用程序而非“被链接版本”的部分源代码。

The “Corresponding Application Code” for a Combined Work means the object code and/or source code for the Application, including any data and utility programs needed for reproducing the Combined Work from the Application, but excluding the System Libraries of the Combined Work.

组合作品的 “对应应用程序代码” 是指应用程序的目标码和/或源代码，包括基于应用程序重新生成组合作品所需的任何数据和实用程序，但不包括组合作品中的系统库。

**1. Exception to Section 3 of the GNU GPL. GNU GPL第3条的例外**

You may convey a covered work under sections 3 and 4 of this License without being bound by section 3 of the GNU GPL.

您可以根据本许可证的第 3 条和第 4 条传递被覆盖的作品，而无需受 GNU GPL 第 3 条的约束。

**2. Conveying Modified Versions. 传递经修改的版本**

If you modify a copy of the Library, and, in your modifications, a facility refers to a function or data to be supplied by an Application that uses the facility (other than as an argument passed when the facility is invoked), then you may convey a copy of the modified version:

如果您修改本库的副本，并且在您的修改中，（本库中的）一个功能调用了使用该功能的应用程序所提供的方法或者数据（而非在调用上述功能时一并将参数上传），那么您可根据以下规定发送修改版本的副本：

- a) under this License, provided that you make a good faith effort to ensure that, in the event an Application does not supply the function or data, the facility still operates, and performs whatever part of its purpose remains meaningful, or
- a) 根据本许可证，只要您善意地努力确保，在应用程序不提供方法或数据的情况下，该功能仍可运行，并且执行该功能的任何部分时仍然具有意义，或 
- b) under the GNU GPL, with none of the additional permissions of this License applicable to that copy.
- b) 以GNU GPL进行传递，本许可证中的任何附加许可均不适用于该副本。

**3. Object Code Incorporating Material from Library Header Files.包含本库头文件材料的目标代码**

The object code form of an Application may incorporate material from a header file that is part of the Library. You may convey such object code under terms of your choice, provided that, if the incorporated material is not limited to numerical parameters, data structure layouts and accessors, or small macros, inline functions and templates (ten or fewer lines in length), you do both of the following:

应用程序的目标代码可能包含作为本库的组成部分的头文件中的材料。如果包含的材料不限于数字参数、数据结构布局和存取器，或小型宏、内联方法和模板（长度少于或等于十行），只要您做到以下两点，则您可以根据您选择的条款传递这些目标代码：

- a) Give prominent notice with each copy of the object code that the Library is used in it and that the Library and its use are covered by this License.
- a) 在发送目标代码的每个副本的同时应提供显著声明，注明其中使用了本库，并且本库及其使用受到本许可证的约束。
- b) Accompany the object code with a copy of the GNU GPL and this license document.
- b）随目标代码提供GNU GPL和本许可证文件的副本。

**4. Combined Works. 组合作品**

You may convey a Combined Work under terms of your choice that, taken together, effectively do not restrict modification of the portions of the Library contained in the Combined Work and reverse engineering for debugging such modifications, if you also do each of the following:

您可以在您选择的条款下传递一个组合作品，这些条款组合在一起能有效地对组合作品中包含的本库的部分内容的修改不予限制，也不限制为调试这类修改所作的反向工程，只要您同时做到下述每一项：

- a) Give prominent notice with each copy of the Combined Work that the Library is used in it and that the Library and its use are covered by this License.
- a) 在发送组合作品的每个副本的同时应提供显著声明，注明其中使用了本库，并且本库及其使用受到本许可证的约束。
- b) Accompany the Combined Work with a copy of the GNU GPL and this license document.
- b) 随组合作品提供GNU GPL和本许可证文件的副本。
- c) For a Combined Work that displays copyright notices during execution, include the copyright notice for the Library among these notices, as well as a reference directing the user to the copies of the GNU GPL and this license document.
- c) 对于在执行过程中显示版权声明的组合作品，在这些声明中包括本库的版权声明，并向用户提供GNU GPL和本许可证文件的查阅位置。
- d) Do one of the following:
- d）做到以下之一：
    - 0) Convey the Minimal Corresponding Source under the terms of this License, and the Corresponding Application Code in a form suitable for, and under terms that permit, the user to recombine or relink the Application with a modified version of the Linked Version to produce a modified Combined Work, in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.
    - 0)根据本许可证条款传递最小对应源代码，并以如下方式传递对应应用程序代码：在形式上适合、且在条款上允许用户重新组合或重新链接应用程序与被链接版本的修改版以生成一份修改后的组合作品，且以上也需遵循GNU GPL第6条规定的传递对应源代码的方式。
    - 1) Use a suitable shared library mechanism for linking with the Library. A suitable mechanism is one that (a) uses at run time a copy of the Library already present on the user's computer system, and (b) will operate properly with a modified version of the Library that is interface-compatible with the Linked Version.
    - 1）使用合适的共享库机制与本库进行链接。合适的机制是指：(a)在运行时使用已经存在于用户电脑系统中本库的副本；并且(b)能够与被链接版本接口兼容的本库的修改版一起正常运行。
- e) Provide Installation Information, but only if you would otherwise be required to provide such information under section 6 of the GNU GPL, and only to the extent that such information is necessary to install and execute a modified version of the Combined Work produced by recombining or relinking the Application with a modified version of the Linked Version. (If you use option 4d0, the Installation Information must accompany the Minimal Corresponding Source and Corresponding Application Code. If you use option 4d1, you must provide the Installation Information in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.)
- e）提供安装信息，但仅根据GNU GPL第6条规定您需要提供这些信息时，并且仅限于这些信息为对于安装和执行组合作品的修改版所必需时，此处组合作品是指由应用程序与被链接版本的修改版重新组合或重新链接而生成。（如果您使用选项4d0，必须随最小对应源代码和对应应用程序代码提供安装信息。如果您使用选项4d1，您必须按照GNU GPL第6条规定的传递对应源代码的方式提供安装信息。）

**5. Combined Libraries. 组合库**

You may place library facilities that are a work based on the Library side by side in a single library together with other library facilities that are not Applications and are not covered by this License, and convey such a combined library under terms of your choice, if you do both of the following:

您可以将基于本库的库功能与其他非应用程序且不受本许可证约束的库功能一起放置在一个单独的库中，并根据您选择的条款传递此组合库，但您必须做到以下两点：

- a) Accompany the combined library with a copy of the same work based on the Library, uncombined with any other library facilities, conveyed under the terms of this License.
- a) 传递组合库时，随附基于本库并且未经与其他库功能组合的上述作品的副本，其传递方式符合本许可的条款。
- b) Give prominent notice with the combined library that part of it is a work based on the Library, and explaining where to find the accompanying uncombined form of the same work.
- b) 传递组合库时，提供显著声明，表明组合库的部分内容是基于本库作品，并且解释何处可以找到未经与其他库功能组合的上述作品。

**6. Revised Versions of the GNU Lesser General Public License. GNU较宽松通用公共许可证的修订版**

The Free Software Foundation may publish revised and/or new versions of the GNU Lesser General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.

自由软件基金会可能会不时发布GNU较宽松通用公共许可证的修订版和/或新版本。这些新版本在精神上与当前版本相似，但在细节上可能有所不同，旨在解决新的问题或关注点。

Each version is given a distinguishing version number. If the Library as you received it specifies that a certain numbered version of the GNU Lesser General Public License “or any later version” applies to it, you have the option of following the terms and conditions either of that published version or of any later version published by the Free Software Foundation. If the Library as you received it does not specify a version number of the GNU Lesser General Public License, you may choose any version of the GNU Lesser General Public License ever published by the Free Software Foundation.

每个版本都有一个不同的版本号。如果您接收的本库指定其适用GNU较宽松通用公共许可证的某个特定版本“或任何后续版本”，您可选择遵循该发布版本或自由软件基金会发布的任何后续版本的条款和条件。如果您接收的本库没有指定GNU较宽松通用公共许可证的版本号，您可以选择自由软件基金会曾就GNU较宽松通用公共许可证发布的任何版本。

If the Library as you received it specifies that a proxy can decide whether future versions of the GNU Lesser General Public License shall apply, that proxy's public statement of acceptance of any version is permanent authorization for you to choose that version for the Library.

如果您接收的本库指定代理方来决定是否适用GNU较宽松通用公共许可证的未来版本，那么该代理器方接受某个版本许可证的公开声明即永久授权您在本库中选择该版本。

---

> _译文声明：本中文译文系基于Peaksol、KUMA、欧轩琦、赵振华的译文投稿，于20231119、20231203经卫sir、沈芬律师、徐美玲助理教授就LGPLv3四份投稿审定, 并经与周明辉教授、张平教授讨论而成。本译文按照CC0授权您使用、复制及传播等。但也请您注意，本译文并非官方译文，仅为便于阅读、理解、研讨而翻译，开放原子开源基金会不提供与本译文相关的任何明示或默示担保。LGPL许可证第3.0版的原文请见：https://www.gnu.org/licenses/lgpl-3.0.en.html_

> _项目介绍：“源译识”翻译项目是由开放原子开源基金会发起的开源公益翻译项目，旨在通过共译凝聚对开源的共识。目前本项目主要涉及开源许可证翻译、开源案例翻译、开源书籍翻译及开源报告翻译等。详情请见：https://atomgit.com/translation._
 