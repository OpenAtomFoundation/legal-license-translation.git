# Contransus™ | Welcome submit your Chinese Translations for FOSS LICENSE! 

- *请您注意，本仓库已搬迁到[新仓库](https://atomgit.com/translation/license-translation)，本仓库已停止维护。*
- *Please kindly notice that, this repo is no longer active, and is replaced and superseded by the [new repo](https://atomgit.com/translation/license-translation).*


The "Contransus" (源译识) Translation Project, initiated by the OpenAtom Foundation, **aims to build open source consensus via translation**, in the hope of developing a widerly-shared open source ecosystem. This project is launched to promote better understanding of open source by offering professionally-reviewed, publicly available and community-oriented translations of various free and open source resources, primarily into Chinese. The project focuses on not only the **translation of open-source licenses**, but also of **open-source-related judgments, books, key reports, etc**.

**Through the [Project Repository](https://atomgit.com/OpenAtomFoundation/translation), you are warmly welcome to participate in the Contransus Project** by using approved translations, filing your translation, sharing your opinion on any translation texts, as well as recommending useful resources and experts in open source domain. If you have any feedback or suggestions, please feel free to contact us via translation@openatom.io.



## Acknowledgements and Promotion

We extend our sincere gratitude to 13 translators: **Lotus、[Vanessa](http://gitee.com/vanessaguo)、[Peaksol](http://gitee.com/peaksol)、[野行僧](http://gitee.com/gzkoala)、[迷糊](http://gitee.com/xriqc)、[赵振华](http://gitee.com/richzhao409)、Sikkim、[Sara](http://gitee.com/zhang-glas)、[OliviaCat](https://gitee.com/oliviacat)、[欧轩琦](https://gitee.com/ouxuanqi)、[KUMA](https://gitee.com/xue-yangjie)、[Leo](https://gitee.com/jin-sihan777)、[Fenju](https://gitee.com/fenju)** and the Review Panel: Prof. ZHOU Minghui, Sir WEI, SHEN Fen, Assistant Prof. XU Meiling, Prof. SONG Lei (listed by order of first submission).