本人LOTUS承诺，该译文系本人原创翻译。本人同意，如本人译文被开放原子开源基金会开源许可证翻译项目选为终版译本，本人同意将译文以CC0协议贡献至公有领域。
heshu_wang@hotmail.com


This is an unofficial translation of the GNU Affero General Public License into Simplified Chinese. It was not published by the Free Software Foundation, and does not legally state the distribution terms for software that uses the GNU AGPL—only the original English text of the GNU AGPL does that. However, we hope that this translation will help Simplified Chinese speakers understand the AGNU GPL better. 

这是 GNU Affero 通用公共许可证（GNU AGPL）的简体中文非官方译文。其并非自由软件基金会发布，也并非使用GNU GPL的软件分发条款的法律文本——只有GNU AGPL的原始英文文本才是具备法律效力的文本。不过，我们希望此份译文能够帮助简体中文用户更好地理解GNU AGPL。



**GNU AFFERO General Public License version 3**
**GNU AFFERO 通用公共许可证第3版**

Version 3, 19 November 2007

第三版，2007年11月9日

Copyright © 2007 Free Software Foundation, Inc. <https://fsf.org/>

版权所有©2007自由软件基金会有限公司。<https://fsf.org/>

Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.

任何人都可以复制和分发本许可证文档的逐字副本，但不允许更改它。



Begin license text.

许可证文本开始。

**Preamble前言**

The GNU Affero General Public License is a free, copyleft license for software and other kinds of works, specifically designed to ensure cooperation with the community in the case of network server software.

GNU通用公共许可证是一个自由的、著佐权型许可证，可用于软件及其他类型作品，特别为针对网络服务器软件情形下保障社区合作而设计。

The licenses for most software and other practical works are designed to take away your freedom to share and change the works. By contrast, our General Public Licenses are intended to guarantee your freedom to share and change all versions of a program--to make sure it remains free software for all its users. 

大多数软件和其他实用作品的许可证都是为剥夺您分享和更改作品的自由而设计。与此不同，我们的通用公共许可证系列旨在保证您共享和更改程序所有版本的自由——确保它对其所有用户都保持是自由软件。

When we speak of free software, we are referring to freedom, not price. Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for them if you wish), that you receive source code or can get it if you want it, that you can change the software or use pieces of it in new free programs, and that you know you can do these things.

当我们谈论自由软件时，我们指的是自由，而不是价格免费。我们的通用公共许可证的设计是为了确保您可以自由地分发自由软件的副本（并对其收费，如果您愿意）、您已得到源代码或者只要您想要就能够获得源代码、您能够修改软件或在新的自由程序中使用其部分，且您知道您可以做这些事情。

Developers that use our General Public Licenses protect your rights with two steps: (1) assert copyright on the software, and (2) offer you this License which gives you legal permission to copy, distribute and/or modify the software.

采用我们的通用公共许可证系列的开发者通过两个步骤来保护您的权利：（1）主张该软件的版权，并（2）依本许可证向您提供合法许可，允许您复制、分发和/或修改该软件。

A secondary benefit of defending all users' freedom is that improvements made in alternate versions of the program, if they receive widespread use, become available for other developers to incorporate. Many developers of free software are heartened and encouraged by the resulting cooperation. However, in the case of software used on network servers, this result may fail to come about. The GNU General Public License permits making a modified version and letting the public access it on a server without ever releasing its source code to the public.

保护全体用户的自由带来的一个好处是该程序的各迭代版本如果被广泛使用，其中所做的改进可以为其他开发者所获取并采用。由此形成的合作令许多自由软件开发者感到振奋和鼓舞。然而，在网络服务器上使用软件的情形下可能不会形成这样的合作。GNU通用公共许可证允许制作修改版本并允许公众通过某一服务器进行访问，而不向公众发布其源代码。

The GNU Affero General Public License is designed specifically to ensure that, in such cases, the modified source code becomes available to the community. It requires the operator of a network server to provide the source code of the modified version running there to the users of that server. Therefore, public use of a modified version, on a publicly accessible server, gives the public access to the source code of the modified version.

GNU Affero通用公共许可证正是为了保证在该情形下社区可以获得修改后的源代码而特别设计。其要求网络服务器的运营者向该服务器的用户提供修改版本的源代码。因此，通过公开可访问的服务器对修改版本进行公开使用时，则需提供公开访问修改版本的源代码的渠道。

An older license, called the Affero General Public License and published by Affero, was designed to accomplish similar goals. This is a different license, not a version of the Affero GPL, but Affero has released a new version of the Affero GPL which permits relicensing under this license.

被称为Affero通用公共许可证并由Affero发布的一个许可证早先版本是为实现类似目的而设计的。这是一个不同的许可证，而不是Affero GPL的某一版本，不过Affero已经另行发布了Affero GPL的新版本，该版本允许基于本许可证进行重新许可。


The precise terms and conditions for copying, distribution and modification follow.

有关复制、分发和修改的确切条款和条件如下。

**TERMS AND CONDITIONS条款和条件**

**0. Definitions. 0.定义。**

“This License” refers to version 3 of the GNU Affero General Public License.

“本许可证”是指GNU Affero通用公共许可证第3版。

“Copyright” also means copyright-like laws that apply to other kinds of works, such as semiconductor masks.

“版权”也指其他类型作品（如半导体掩模）所适用的类似版权的法律。

“The Program” refers to any copyrightable work licensed under this License. Each licensee is addressed as “you”. “Licensees” and “recipients” may be individuals or organizations.

“本程序”是指在本许可证下许可的任何可版权作品。每个被许可人均被称为“您”。“被许可人”和“接收者”可以是个人或组织。

To “modify” a work means to copy from or adapt all or part of the work in a fashion requiring copyright permission, other than the making of an exact copy. The resulting work is called a “modified version” of the earlier work or a work “based on” the earlier work.

“修改”作品是指以需要版权许可的方式复制或改编全部或部分作品，制作完全相同副本的情况除外。由此产生的作品被称为原先作品的“修改版本”或“基于”原先作品的作品。

A “covered work” means either the unmodified Program or a work based on the Program.

“所覆盖作品”是指未经修改的本程序或基于本程序的作品。

To “propagate” a work means to do anything with it that, without permission, would make you directly or secondarily liable for infringement under applicable copyright law, except executing it on a computer or modifying a private copy. Propagation includes copying, distribution (with or without modification), making available to the public, and in some countries other activities as well.

“传播”作品意味着您在未经允许的情况下将使您直接或次要承担根据所适用版权法构成侵权的行为，在电脑上运行或修改私有副本的情形除外。传播包括复制、分发（修改或未修改的）、向公众提供，以及（在有些国家还有）其他行为。

To “convey” a work means any kind of propagation that enables other parties to make or receive copies. Mere interaction with a user through a computer network, with no transfer of a copy, is not conveying.

“传递”作品是指使其他方能够制作或接收副本的任何传播。仅仅通过计算机网络与用户进行交互，而不转移副本，不属于传递。

An interactive user interface displays “Appropriate Legal Notices” to the extent that it includes a convenient and prominently visible feature that (1) displays an appropriate copyright notice, and (2) tells the user that there is no warranty for the work (except to the extent that warranties are provided), that licensees may convey the work under this License, and how to view a copy of this License. If the interface presents a list of user commands or options, such as a menu, a prominent item in the list meets this criterion.

交互式用户界面显示“适当的法律声明”的应有程度是其包含方便且显著可见的特性（1）显示了适当的版权声明，并（2）告诉了用户：作品并无保证（已提供的保证除外），被许可方可以根据本许可证传递本作品，以及如何查看本许可证的副本。如果该界面显示的是用户命令或选项的列表，如菜单，则列表中的突出显示条目即可满足此标准。

**1. Source Code. 1.源代码**

The “source code” for a work means the preferred form of the work for making modifications to it. “Object code” means any non-source form of a work.

作品的“源代码”是指对该作品进行修改的首选形式。“目标代码”是指作品的任何非源代码形式。

A “Standard Interface” means an interface that either is an official standard defined by a recognized standards body, or, in the case of interfaces specified for a particular programming language, one that is widely used among developers working in that language.

“标准接口”是指由公认的标准机构定义的官方标准，或者，在为特定编程语言指定的接口的情形下，是指使用该语言的开发者普遍使用的接口。

The “System Libraries” of an executable work include anything, other than the work as a whole, that (a) is included in the normal form of packaging a Major Component, but which is not part of that Major Component, and (b) serves only to enable use of the work with that Major Component, or to implement a Standard Interface for which an implementation is available to the public in source code form. A “Major Component”, in this context, means a major essential component (kernel, window system, and so on) of the specific operating system (if any) on which the executable work runs, or a compiler used to produce the work, or an object code interpreter used to run it.

可执行作品的“系统库”包括（不同于作为整体的作品） (a)以正常形式打包主要组件的任何内容，但其并不是主要组件的一部分，和(b)仅为使作品用于主要组件的任何内容，或为实现一个标准接口而以源代码形式向公众提供的实现的任何内容。“主要组件”在此处是指可执行作品所运行的特定操作系统（如有）的主要基本组件（内核、窗口系统等），或用于生成该作品的编译器，或用于运行其的目标代码解释器。

The “Corresponding Source” for a work in object code form means all the source code needed to generate, install, and (for an executable work) run the object code and to modify the work, including scripts to control those activities. However, it does not include the work's System Libraries, or general-purpose tools or generally available free programs which are used unmodified in performing those activities but which are not part of the work. For example, Corresponding Source includes interface definition files associated with source files for the work, and the source code for shared libraries and dynamically linked subprograms that the work is specifically designed to require, such as by intimate data communication or control flow between those subprograms and other parts of the work.

目标代码形式作品的“对应源代码”是指生成、安装和（为可执行作品）运行目标代码和修改作品所需的所有源代码，包括控制这些动作的脚本。但是，它不包括作品的系统库，或在执行这些动作时会被不经修改地使用、但不属于作品一部分的通用工具或通常可用的自由程序。例如，对应源代码包括与作品源文件相关联的接口定义文件，以及专为该作品所设计的共享库和动态链接子程序的源代码，例如在这些子程序和该作品其他部分之间进行亲密数据通信或控制流。

The Corresponding Source need not include anything that users can regenerate automatically from other parts of the Corresponding Source.

对应源代码不需包含用户能够从对应源代码其他部分自动重新生成的任何内容。

The Corresponding Source for a work in source code form is that same work.

源代码形式作品的对应源代码是该相同作品。

**2. Basic Permissions.2.基本许可证。**

All rights granted under this License are granted for the term of copyright on the Program, and are irrevocable provided the stated conditions are met. This License explicitly affirms your unlimited permission to run the unmodified Program. The output from running a covered work is covered by this License only if the output, given its content, constitutes a covered work. This License acknowledges your rights of fair use or other equivalent, as provided by copyright law.

本许可证项下授予的所有权利均在本程序的版权期限内授予，且如果满足所述条件则不可撤销。本许可证明示确认您有不受限的权利运行未经修改的程序。运行所覆盖作品的输出只在该输出就其内容来看构成所覆盖作品时，才受本许可证的约束。本许可证承认您享有版权法下合理使用的权利或其他同等权利。

You may make, run and propagate covered works that you do not convey, without conditions so long as your license otherwise remains in force. You may convey covered works to others for the sole purpose of having them make modifications exclusively for you, or provide you with facilities for running those works, provided that you comply with the terms of this License in conveying all material for which you do not control copyright. Those thus making or running the covered works for you must do so exclusively on your behalf, under your direction and control, on terms that prohibit them from making any copies of your copyrighted material outside their relationship with you.

您可以制作、运行和传播您未对外传递的所覆盖作品，只要您的许可证仍然保持有效，就没有其他前提条件。您可以仅为让他人专门替您修改的目的、或仅为让他人给您提供设施运行这些作品的目的，向他们传递所覆盖作品，只要您在传递您不控制版权的所有材料时遵守本许可证的条款。那些为您制作或运行所覆盖作品的人行事必须仅只是为代表您，在您的指导和控制下，并禁止他们在与您的关系之外复制您受版权保护的材料。

Conveying under any other circumstances is permitted solely under the conditions stated below. Sublicensing is not allowed; section 10 makes it unnecessary. 

在任何其他情况下的传递，都只允许在以下条件下进行。再许可不被允许；第10条使之变得非必要。

**3. Protecting Users' Legal Rights From Anti-Circumvention Law.3. 保护用户的合法权利不受反规避法的侵害。**

No covered work shall be deemed part of an effective technological measure under any applicable law fulfilling obligations under article 11 of the WIPO copyright treaty adopted on 20 December 1996, or similar laws prohibiting or restricting circumvention of such measures.

所覆盖的作品不得被视为有效技术措施的一部分，不论是根据为满足1996年12月20日通过的世界知识产权组织版权条约第11条规定义务而制定的任何法律，还是根据禁止或限制规避这些措施的类似法律。

When you convey a covered work, you waive any legal power to forbid circumvention of technological measures to the extent such circumvention is effected by exercising rights under this License with respect to the covered work, and you disclaim any intention to limit operation or modification of the work as a means of enforcing, against the work's users, your or third parties' legal rights to forbid circumvention of technological measures.

当您传递所覆盖作品时，您放弃任何禁止规避技术措施的法律权力，只要这种规避是通过行使本许可证对所覆盖作品的权利实现，并且您否认将限制操作或修改作品作为针对作品用户实现您或第三方禁止规避技术措施的合法权利的方式的任何意图。

**4. Conveying Verbatim Copies.4. 传递逐字副本。**

You may convey verbatim copies of the Program's source code as you receive it, in any medium, provided that you conspicuously and appropriately publish on each copy an appropriate copyright notice; keep intact all notices stating that this License and any non-permissive terms added in accord with section 7 apply to the code; keep intact all notices of the absence of any warranty; and give all recipients a copy of this License along with the Program.

您可以通过任何媒体传递您所收到的本程序源代码的逐字副本，但您可以在每份副本上显著和适当地发布适当的版权声明；完整保留代码适用本许可证及根据第7条添加的任何非宽松型条款的所有声明；完整保留所有没有任何保证的声明；并向所有接收者提供本许可证的副本。

You may charge any price or no price for each copy that you convey, and you may offer support or warranty protection for a fee.

您可以对您传递的每份副本收费或不收费，您也可以收费提供支持或保修保护。

**5. Conveying Modified Source Versions.5. 传递修改后的源代码版本。**

You may convey a work based on the Program, or the modifications to produce it from the Program, in the form of source code under the terms of section 4, provided that you also meet all of these conditions:

您可以根据第4条以源代码的形式传递基于本程序的作品，或从本程序生成前述作品的修改，前提是您也满足所有以下条件：

a) The work must carry prominent notices stating that you modified it, and giving a relevant date.

a)该作品必须带有突出的声明，说明您修改了它，并给出一个相关的日期。

b) The work must carry prominent notices stating that it is released under this License and any conditions added under section 7. This requirement modifies the requirement in section 4 to “keep intact all notices”.

b)该作品必须带有突出的声明，说明它是根据本许可证以及任何根据第7条添加的条件所发布。本条要求构成对第4条中关于“完整保留所有声明”要求的变更。

c) You must license the entire work, as a whole, under this License to anyone who comes into possession of a copy. This License will therefore apply, along with any applicable section 7 additional terms, to the whole of the work, and all its parts, regardless of how they are packaged. This License gives no permission to license the work in any other way, but it does not invalidate such permission if you have separately received it.

c)您必须在本许可证下将全部作品，作为一个整体，授权给拥有副本的任何人。因此本许可证与根据第7条而附加的任何条款一起，将适用于整个作品，及其所有部分，无论它们是如何打包的。本许可证不允许以任何其他方式许可该作品，但不会使您单独接收的许可无效。

d) If the work has interactive user interfaces, each must display Appropriate Legal Notices; however, if the Program has interactive interfaces that do not display Appropriate Legal Notices, your work need not make them do so.

d)如果作品具有交互式用户界面，每个界面都必须显示适当的法律声明；但是，如果本程序的交互式界面原本并不显示适当的法律声明，您的作品也无需让它们这样做。

A compilation of a covered work with other separate and independent works, which are not by their nature extensions of the covered work, and which are not combined with it such as to form a larger program, in or on a volume of a storage or distribution medium, is called an “aggregate” if the compilation and its resulting copyright are not used to limit the access or legal rights of the compilation's users beyond what the individual works permit. Inclusion of a covered work in an aggregate does not cause this License to apply to the other parts of the aggregate.

所覆盖作品与其他单独且独立的作品（其在本质上不属于所覆盖作品的扩展，且未与之结合以构成一个更大的程序）汇编于同一存储或分发介质中的，被称为“聚合体”——如果该汇编内容及其产生的版权并不限制用户对汇编内容的访问或合法权利至于超出单个作品许可的限度。将所覆盖作品置于聚合体中并不会导致本许可证适用于聚合体的其他部分。

**6. Conveying Non-Source Forms.6.传递非源代码形式。**

You may convey a covered work in object code form under the terms of sections 4 and 5, provided that you also convey the machine-readable Corresponding Source under the terms of this License, in one of these ways:

您可以根据第4条和第5条以目标代码的形式传递所覆盖作品，前提是您也在本许可证条款下通过以下方式之一传递机器可读的对应源代码：

a) Convey the object code in, or embodied in, a physical product (including a physical distribution medium), accompanied by the Corresponding Source fixed on a durable physical medium customarily used for software interchange.

a)传递包含或内置目标代码的实物产品（包括实物分发介质），同时随附固定于通常用于软件交换的实物介质上的对应源代码。

b) Convey the object code in, or embodied in, a physical product (including a physical distribution medium), accompanied by a written offer, valid for at least three years and valid for as long as you offer spare parts or customer support for that product model, to give anyone who possesses the object code either (1) a copy of the Corresponding Source for all the software in the product that is covered by this License, on a durable physical medium customarily used for software interchange, for a price no more than your reasonable cost of physically performing this conveying of source, or (2) access to copy the Corresponding Source from a network server at no charge.

b)传递包含或内置目标代码的实物产品（包括实物分发介质），同时随附一份书面要约——至少三年有效、且只在您提供该型号产品的备件或客户支持的情形下有效——给任何拥有目标代码的人（1）固定于通常用于软件交换的实物介质上的、该产品中本许可证所覆盖的所有软件的一份对应源代码副本，可收取不超过您实际执行源代码传递的合理成本费用，或（2）从网络服务器复制对应源代码副本的访问权限，不得收费。

c) Convey individual copies of the object code with a copy of the written offer to provide the Corresponding Source. This alternative is allowed only occasionally and noncommercially, and only if you received the object code with such an offer, in accord with subsection 6b.

c)将目标代码的单独副本与提供对应源代码的书面要约副本一起传递。此替代方案仅可偶尓在非商业情形下采用，并且仅适用于您根据第6条b款接收的目标代码随附该书面要求的情形。

d) Convey the object code by offering access from a designated place (gratis or for a charge), and offer equivalent access to the Corresponding Source in the same way through the same place at no further charge. You need not require recipients to copy the Corresponding Source along with the object code. If the place to copy the object code is a network server, the Corresponding Source may be on a different server (operated by you or a third party) that supports equivalent copying facilities, provided you maintain clear directions next to the object code saying where to find the Corresponding Source. Regardless of what server hosts the Corresponding Source, you remain obligated to ensure that it is available for as long as needed to satisfy these requirements.

d)通过提供从指定地点的访问权限（免费或收费）来传递目标代码，并以相同方式提供对应源代码的同等访问权限，不再进一步收费。您无需要求接收者在复制目标代码的同时复制对应源代码。如果复制目标代码的位置是某个网络服务器，则对应源代码可以置于支持同等复制设施的不同服务器（由您或第三方操作）上，前提是您紧随目标代码给出清晰的指引，说明在哪里可以找到对应源代码。无论哪个服务器承载对应源代码，您都有义务确保它按需可用以满足这些要求。

e) Convey the object code using peer-to-peer transmission, provided you inform other peers where the object code and Corresponding Source of the work are being offered to the general public at no charge under subsection 6d.

e)通过点对点传输方式传递目标代码，前提是您通知其他提供目标代码和对应源代码下载点根据第6条d款免费向公众提供。

A separable portion of the object code, whose source code is excluded from the Corresponding Source as a System Library, need not be included in conveying the object code work.

目标代码中可分离的部分，其源代码作为系统库被排除在对应源代码外，无需包含在目标代码形式作品的传递中。

A “User Product” is either (1) a “consumer product”, which means any tangible personal property which is normally used for personal, family, or household purposes, or (2) anything designed or sold for incorporation into a dwelling. In determining whether a product is a consumer product, doubtful cases shall be resolved in favor of coverage. For a particular product received by a particular user, “normally used” refers to a typical or common use of that class of product, regardless of the status of the particular user or of the way in which the particular user actually uses, or expects or is expected to use, the product. A product is a consumer product regardless of whether the product has substantial commercial, industrial or non-consumer uses, unless such uses represent the only significant mode of use of the product.

“用户产品”是（1）“消费品”，指通常用于个人、家庭或家庭目的的任何有形的个人财产，或者（2）为合入居所而设计或销售的任何产品。在确定一种产品是否为消费品时，对可疑情况应按有利于覆盖范围的角度解决。对于特定用户收到的特定产品，“通常使用”指的是该类产品的典型或常见的使用，而不论该特定用户的状态或该特定用户对该产品实际使用的方式、或者期望或被期望使用的方式。产品属于消费产品的认定不考虑该产品是否存在大量的商业、工业或非消费用途，除非该用途是该产品唯一重要的使用方式。

“Installation Information” for a User Product means any methods, procedures, authorization keys, or other information required to install and execute modified versions of a covered work in that User Product from a modified version of its Corresponding Source. The information must suffice to ensure that the continued functioning of the modified object code is in no case prevented or interfered with solely because modification has been made.

用户产品的“安装信息”是指基于对应源代码的修改版本安装和执行该用户产品中的所覆盖作品修改版本所需的任何方法、程序、授权密钥或其他信息。这些信息必须足以确保修改后目标代码的持续运行在任何情况下都不会仅仅因为进行过修改而被阻止或干扰。

If you convey an object code work under this section in, or with, or specifically for use in, a User Product, and the conveying occurs as part of a transaction in which the right of possession and use of the User Product is transferred to the recipient in perpetuity or for a fixed term (regardless of how the transaction is characterized), the Corresponding Source conveyed under this section must be accompanied by the Installation Information. But this requirement does not apply if neither you nor any third party retains the ability to install modified object code on the User Product (for example, the work has been installed in ROM).

如果您传递本条下的目标代码作品，该作品在用户产品中、或与用户产品一起、或专门用于用户产品，并且传递作为交易的一部分发生，其中用户产品的所有权和使用权被永久或固定期限（无论交易的特征如何）转让给接收者，则本条下传递的对应源代码必须附有安装信息。但是，如果您或任何第三方都没有保留在用户产品上安装修改后的目标代码的能力(例如，该作品已被安装在ROM中)，则此要求不适用。

The requirement to provide Installation Information does not include a requirement to continue to provide support service, warranty, or updates for a work that has been modified or installed by the recipient, or for the User Product in which it has been modified or installed. Access to a network may be denied when the modification itself materially and adversely affects the operation of the network or violates the rules and protocols for communication across the network.

提供安装信息的要求并不包括为已被接收者修改或安装的作品、或为修改或安装有该作品的用户产品持续提供支持服务、保证或更新的要求。当修改本身对网络的运行产生重大不利影响或违反了跨网络通信的规则和协议时，对网络的访问可能会被拒绝。

Corresponding Source conveyed, and Installation Information provided, in accord with this section must be in a format that is publicly documented (and with an implementation available to the public in source code form), and must require no special password or key for unpacking, reading or copying.

根据本条传递的对应源代码和提供的安装信息必须是公开记录的格式（并以源代码形式向公众提供实现），并且不需要解包、读取或复制的特殊密码或密钥。

**7. Additional Terms.7. 附加条款。**

“Additional permissions” are terms that supplement the terms of this License by making exceptions from one or more of its conditions. Additional permissions that are applicable to the entire Program shall be treated as though they were included in this License, to the extent that they are valid under applicable law. If additional permissions apply only to part of the Program, that part may be used separately under those permissions, but the entire Program remains governed by this License without regard to the additional permissions.

“附加权限”是通过对本许可证的一个或多个条件设置例外来补充本许可证条款的条款。附加权限适用于本程序整体的，其应被视同包含于本许可证中，按所适用法律确认有效程度。如果附加权限仅适用于本程序的一部分，则该部分可以在这些权限下单独使用，但本程序整体仍受本许可证约束而不考虑附加权限。

When you convey a copy of a covered work, you may at your option remove any additional permissions from that copy, or from any part of it. (Additional permissions may be written to require their own removal in certain cases when you modify the work.) You may place additional permissions on material, added by you to a covered work, for which you have or can give appropriate copyright permission.

当您传递所覆盖作品的副本时，您可以选择从该副本或其任何部分删除任何附加权限。（附加权限可能会书面要求在某些情况下如您修改作品，须删除附加条款。）您可以对由您添加到所覆盖作品（您对其已拥有或可以给予适当的版权许可）中的内容设置附加权限。

Notwithstanding any other provision of this License, for material you add to a covered work, you may (if authorized by the copyright holders of that material) supplement the terms of this License with terms:

不论本许可证的任何其他规定，对于您在所覆盖作品中添加的内容，您可以（如经该材料的版权所有者授权）用以下条款来补充本许可证的条款：

a) Disclaiming warranty or limiting liability differently from the terms of sections 15 and 16 of this License; or

a) 与本许可证第15条和第16条不同的声明无担保或责任限制条款；或

b) Requiring preservation of specified reasonable legal notices or author attributions in that material or in the Appropriate Legal Notices displayed by works containing it; or

b) 要求在该材料中或在包含该材料的作品所显示的适当法律声明中保留特定的合理法律声明或作者署名；或

c) Prohibiting misrepresentation of the origin of that material, or requiring that modified versions of such material be marked in reasonable ways as different from the original version; or

c) 禁止虚假陈述该材料的来源，或要求该材料的修改版本以与原始版本不同的合理方式进行标记；或

d) Limiting the use for publicity purposes of names of licensors or authors of the material; or

d) 限制将许可人或该材料作者的姓名用于宣传目的；或

e) Declining to grant rights under trademark law for use of some trade names, trademarks, or service marks; or

e) 拒绝根据商标法授予使用某些商号、商标或服务标识的权利；或

f) Requiring indemnification of licensors and authors of that material by anyone who conveys the material (or modified versions of it) with contractual assumptions of liability to the recipient, for any liability that these contractual assumptions directly impose on those licensors and authors.

f) 要求许可人和作者对向接收者传递该材料（或修改后的材料）的任何人基于合同假设的责任进行赔偿，不论是基于这些合同假设直接强加于许可人和作者的任何责任。

All other non-permissive additional terms are considered “further restrictions” within the meaning of section 10. If the Program as you received it, or any part of it, contains a notice stating that it is governed by this License along with a term that is a further restriction, you may remove that term. If a license document contains a further restriction but permits relicensing or conveying under this License, you may add to a covered work material governed by the terms of that license document, provided that the further restriction does not survive such relicensing or conveying.

所有其他非宽松型附加条款均被视为第10条意义上的“进一步限制”。如果您接收的本程序或其任何部分包含一个声明，称其受本许可证及某一构成进一步限制的条款约束，您可以删除该条款。如果一个许可文件包含进一步限制但允许在本许可证下进行重新许可或传递，您可以向所覆盖作品中添加受该许可文件条款约束的材料，只要该进一步限制在该重新许可或传递时不复存在。

If you add terms to a covered work in accord with this section, you must place, in the relevant source files, a statement of the additional terms that apply to those files, or a notice indicating where to find the applicable terms.

如果您在根据本条向所覆盖作品中附加条款，您必须在相关源文件中放置适用于这些文件的附加条款的声明，或放置指出在哪里能找到适用条款的声明。

Additional terms, permissive or non-permissive, may be stated in the form of a separately written license, or stated as exceptions; the above requirements apply either way.

附加条款，宽松型或非宽松型，可以以单独的书面许可形式声明，或作为例外情况形式声明；不论任一形式上述要求均适用。

**8. Termination.8. 终止。**

You may not propagate or modify a covered work except as expressly provided under this License. Any attempt otherwise to propagate or modify it is void, and will automatically terminate your rights under this License (including any patent licenses granted under the third paragraph of section 11).

除非在本许可证项下有明确规定，否则您不得传播或修改所覆盖作品。任何试图对其进行传播或修改的尝试都是无效的，并将自动终止您在本许可证下的权利（包括根据第11条第三段授予的任何专利许可）。

However, if you cease all violation of this License, then your license from a particular copyright holder is reinstated (a) provisionally, unless and until the copyright holder explicitly and finally terminates your license, and (b) permanently, if the copyright holder fails to notify you of the violation by some reasonable means prior to 60 days after the cessation.

然而，如果您停止所有违反本许可证的行为，那么授予您的许可从特定版权持有人处被 (a)暂时性恢复，除非且直到版权持有人明示并最终终止授予您的许可，和(b)永久性恢复，如果版权持有人未能通过某种合理方式在您停止违反行为60天之内向您发出违反通知。

Moreover, your license from a particular copyright holder is reinstated permanently if the copyright holder notifies you of the violation by some reasonable means, this is the first time you have received notice of violation of this License (for any work) from that copyright holder, and you cure the violation prior to 30 days after your receipt of the notice.

此外，授予您的许可从特定版权持有人处被永久性恢复，如果该版权持有人通过某种合理方式向您发出了违反通知，而这是您首次收到该版权持有人发出的（因任何作品）违反本许可证的通知，且您在收到通知30天之内停止了违反行为。

Termination of your rights under this section does not terminate the licenses of parties who have received copies or rights from you under this License. If your rights have been terminated and not permanently reinstated, you do not qualify to receive new licenses for the same material under section 10.

在本条下终止您的权利并不终止从您处根据本许可证接收到副本或权利的各当事方的许可。如果您的权利被终止且未被永久性恢复，则您并无资格根据第10条就相同材料接收新的许可。

**9. Acceptance Not Required for Having Copies.9.持有副本无须承诺接受。**

You are not required to accept this License in order to receive or run a copy of the Program. Ancillary propagation of a covered work occurring solely as a consequence of using peer-to-peer transmission to receive a copy likewise does not require acceptance. However, nothing other than this License grants you permission to propagate or modify any covered work. These actions infringe copyright if you do not accept this License. Therefore, by modifying or propagating a covered work, you indicate your acceptance of this License to do so.

您无需为接收或运行本程序的副本而承诺接受本许可证。仅基于点对点传输来接收所覆盖作品副本造成的辅助性传播同样不需要承诺接受。但是，除本许可证之外，并无任何允许您传播或修改任何所覆盖作品的许可。如果您不承诺接受本许可证，则这些行为是侵犯版权的。因此，通过修改或传播所覆盖作品的行为，您表示您承诺接受此许可。

**10. Automatic Licensing of Downstream Recipients.10. 对下游接收者的自动许可。**

Each time you convey a covered work, the recipient automatically receives a license from the original licensors, to run, modify and propagate that work, subject to this License. You are not responsible for enforcing compliance by third parties with this License.

每次您传递所覆盖作品时，接收者会自动从原始许可方获得许可，以根据本许可证运行、修改和传播该作品。您无需负责强制执行第三方遵守本许可证。

An “entity transaction” is a transaction transferring control of an organization, or substantially all assets of one, or subdividing an organization, or merging organizations. If propagation of a covered work results from an entity transaction, each party to that transaction who receives a copy of the work also receives whatever licenses to the work the party's predecessor in interest had or could give under the previous paragraph, plus a right to possession of the Corresponding Source of the work from the predecessor in interest, if the predecessor has it or can get it with reasonable efforts.

“实体交易”是指转移一个组织、或其实质上的所有资产的控制权，或是对一个组织进行拆分，或对多个组织进行兼并。如果因实体交易而造成所覆盖作品的传播，接收到该作品的交易各方除收到其利益相关的前任根据前述段落所拥有或可以获得的任何许可外，还可以从其利益相关的前任处取得该作品对应源代码的占有权，如果该前任确实拥有或能够通过合理的努力得到该权利。

You may not impose any further restrictions on the exercise of the rights granted or affirmed under this License. For example, you may not impose a license fee, royalty, or other charge for exercise of rights granted under this License, and you may not initiate litigation (including a cross-claim or counterclaim in a lawsuit) alleging that any patent claim is infringed by making, using, selling, offering for sale, or importing the Program or any portion of it.

您不得就本许可证项下授予或确认的权利的行使施加任何进一步限制。例如，您不得就本许可证下授予权利的行使收取授权费、许可费或其他费用，您也不得提起诉讼（包括法律诉讼中的交叉索赔或反诉）主张本程序或其任何部分的制造、使用、销售、出售或进口侵犯任何专利权利要求。

**11. Patents.11.专利。**

A “contributor” is a copyright holder who authorizes use under this License of the Program or a work on which the Program is based. The work thus licensed is called the contributor's “contributor version”.

“贡献者”是指根据本许可证授权使用本程序或本程序所基于的作品的版权持有人。因此被许可的作品被称为贡献者的“贡献者版本”。

A contributor's “essential patent claims” are all patent claims owned or controlled by the contributor, whether already acquired or hereafter acquired, that would be infringed by some manner, permitted by this License, of making, using, or selling its contributor version, but do not include claims that would be infringed only as a consequence of further modification of the contributor version. For purposes of this definition, “control” includes the right to grant patent sublicenses in a manner consistent with the requirements of this License.

贡献者的“必要专利权利要求”是指贡献者拥有或控制的、无论已取得或后续将取得、以经本许可证允许的某种形式制造、使用或销售其贡献者版本而将被侵犯的所有专利权利要求，但不包括仅因贡献者版本的进一步修改而将被侵犯的权利要求。在本定义中，“控制”包括以符合本许可证要求的方式授予专利再许可的权利。

Each contributor grants you a non-exclusive, worldwide, royalty-free patent license under the contributor's essential patent claims, to make, use, sell, offer for sale, import and otherwise run, modify and propagate the contents of its contributor version.

每个贡献者就其必要专利权利要求，授予您一个非排他的、全球范围的、免许可费的专利许可，以制造、使用、销售、许诺销售、进口和以其他方式运行、修改和传播其贡献者版本的内容。

In the following three paragraphs, a “patent license” is any express agreement or commitment, however denominated, not to enforce a patent (such as an express permission to practice a patent or covenant not to sue for patent infringement). To “grant” such a patent license to a party means to make such an agreement or commitment not to enforce a patent against the party.

以下三段落中的“专利许可”是任何明示不强制执行专利的协议或承诺（比如明确允许实施专利或承诺不起诉专利侵权），无论其如何命名。向当事方“授予”专利许可是指作出同意不针对该当事方强制执行专利的协议或承诺。

If you convey a covered work, knowingly relying on a patent license, and the Corresponding Source of the work is not available for anyone to copy, free of charge and under the terms of this License, through a publicly available network server or other readily accessible means, then you must either (1) cause the Corresponding Source to be so available, or (2) arrange to deprive yourself of the benefit of the patent license for this particular work, or (3) arrange, in a manner consistent with the requirements of this License, to extend the patent license to downstream recipients. “Knowingly relying” means you have actual knowledge that, but for the patent license, your conveying the covered work in a country, or your recipient's use of the covered work in a country, would infringe one or more identifiable patents in that country that you have reason to believe are valid.

如果您传递所覆盖作品，明知依赖某一专利许可，且该作品的对应源代码不能被获取供任何人复制——免费且在本许可证条款下——通过公开网络服务器或其他容易访问的手段，则您必须（1）使对应源代码可如此获取，或者（2）对您自己就这一特定作品的专利许可利益作出剥夺安排，或者（3）以符合本许可证要求的方式，安排将专利许可扩展到下游接收者。“明知依赖”意味着您实际知晓，除非取得该专利许可，您在某一地域内传递所覆盖作品，或您的接收者在该地域内使用所覆盖作品，将侵犯该地域内一个或多个可识别的、您有理由认为有效的专利。

If, pursuant to or in connection with a single transaction or arrangement, you convey, or propagate by procuring conveyance of, a covered work, and grant a patent license to some of the parties receiving the covered work authorizing them to use, propagate, modify or convey a specific copy of the covered work, then the patent license you grant is automatically extended to all recipients of the covered work and works based on it.

如果依照或根据某一交易或安排，您传递、或通过推动传递来传播所覆盖作品，并向某些接收所覆盖作品的当事方授予专利许可，授权他们使用、传播、修改或传递该所覆盖作品的特定副本，那么您授予的专利许可将自动扩展授予给该所覆盖作品和基于其的作品的所有接收者。

A patent license is “discriminatory” if it does not include within the scope of its coverage, prohibits the exercise of, or is conditioned on the non-exercise of one or more of the rights that are specifically granted under this License. You may not convey a covered work if you are a party to an arrangement with a third party that is in the business of distributing software, under which you make payment to the third party based on the extent of your activity of conveying the work, and under which the third party grants, to any of the parties who would receive the covered work from you, a discriminatory patent license (a) in connection with copies of the covered work conveyed by you (or copies made from those copies), or (b) primarily for and in connection with specific products or compilations that contain the covered work, unless you entered into that arrangement, or that patent license was granted, prior to 28 March 2007.

如果某一专利许可不在其覆盖范围内禁止行使、或以不行使本许可证下明确授予的一项或多项权利为条件，则该专利许可是“歧视性的”。如果您与从事分发软件业务的第三方达成某一安排，据此安排您基于您传递所覆盖作品行为的程度向第三方支付款项，且据此安排第三方向任何从您处接收所覆盖作品的当事方授予(a)与您传递的所覆盖作品的副本（或基于这些副本制作的副本）相关联的歧视性的专利许可，或者(b)主要是为包含所覆盖作品的特定产品或汇编而作出且与之相关联的歧视性的专利许可，则您不能传递所覆盖作品，除非在2007年3月28日之前您已达成了该安排，或已授予该专利许可。

Nothing in this License shall be construed as excluding or limiting any implied license or other defenses to infringement that may otherwise be available to you under applicable patent law.

本许可证中的任何内容均不得被解释为排除或限制您根据适用的专利法可能取得的任何默示许可或其他侵权抗辩。

**12. No Surrender of Others' Freedom.12.不牺牲他人的自由。**

If conditions are imposed on you (whether by court order, agreement or otherwise) that contradict the conditions of this License, they do not excuse you from the conditions of this License. If you cannot convey a covered work so as to satisfy simultaneously your obligations under this License and any other pertinent obligations, then as a consequence you may not convey it at all. For example, if you agree to terms that obligate you to collect a royalty for further conveying from those to whom you convey the Program, the only way you could satisfy both those terms and this License would be to refrain entirely from conveying the Program.

如果您被施加的条件（无论是通过法院命令、协议或其他方式）与本许可证的条件相矛盾，它们不能成为您不履行本许可证条件的借口。如果您不能在同时满足您在本许可证下的义务和任何其他相关义务的情况下传递所覆盖作品，则您可能根本无权传递它。例如，如果您同意了某些条款，其使您有义务对接收您传递的本程序的人的进一步传递行为收取许可费，那么您能够满足这些条款和本许可证的唯一方法是完全避免传递本程序。

**13. Remote Network Interaction; Use with the GNU General Public License.13.远程网络交互；与GNU 通用公共许可证共同使用。**

Notwithstanding any other provision of this License, if you modify the Program, your modified version must prominently offer all users interacting with it remotely through a computer network (if your version supports such interaction) an opportunity to receive the Corresponding Source of your version by providing access to the Corresponding Source from a network server at no charge, through some standard or customary means of facilitating copying of software. This Corresponding Source shall include the Corresponding Source for any work covered by version 3 of the GNU General Public License that is incorporated pursuant to the following paragraph.

不论本许可证有其他任何条款，如您修改本程序，则必须向通过计算机网络与之进行远程交互（如您的版本支持该种交互）的所有用户提供接收您所修改版本的对应源代码的机会——通过网络服务器免费提供对应源代码的访问途径，并以便于软件复制的某些标准或惯常的方式提供。对应源代码中应当包含根据以下段落合入、被GNU通用公共许可证所覆盖的任何作品的对应源代码。

Notwithstanding any other provision of this License, you have permission to link or combine any covered work with a work licensed under version 3 of the GNU General Public License into a single combined work, and to convey the resulting work. The terms of this License will continue to apply to the part which is the covered work, but the work with which it is combined will remain governed by version 3 of the GNU General Public License.

不论本许可证有任何其他条款，您有权将任何所覆盖作品与GNU 通用公共许可证第3版下许可的作品链接或组合为单一组合作品，并对由此产生的作品进行传递。本许可证条款将继续适用于所覆盖作品，但与之组合的原作品仍将受GNU 通用公共许可证第3版的约束。

**14. Revised Versions of this License.14. 本许可证的修订版本。**

The Free Software Foundation may publish revised and/or new versions of the GNU Affero General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.

自由软件基金会可能会不时发布GNU Affero通用公共许可证的修订和/或更新版本。这些新版本将在精神上与当前版本相似，但可能在细节上有所不同以解决新问题或关切。

Each version is given a distinguishing version number. If the Program specifies that a certain numbered version of the GNU Affero General Public License “or any later version” applies to it, you have the option of following the terms and conditions either of that numbered version or of any later version published by the Free Software Foundation. If the Program does not specify a version number of the GNU Affero General Public License, you may choose any version ever published by the Free Software Foundation.

每个版本都有一个不同的版本号。如果本程序指定某个特定版本的GNU Affero通用公共许可证版本“或任何以后版本”适用于它，您可以选择遵循该编号版本或自由软件基金会发布的任何后续版本的条款和条件。如果本程序没有指定GNU Affero通用公共许可证的版本号，则您可以选择自由软件基金会曾经发布过的任何版本。

If the Program specifies that a proxy can decide which future versions of the GNU Affero General Public License can be used, that proxy's public statement of acceptance of a version permanently authorizes you to choose that version for the Program.

如果本程序指定一个代理方来决定使用GNU Affero通用公共许可证的哪一将来版本，则该代理方对某一版本的公开接受声明将永久地授权您为本程序选择该版本。

Later license versions may give you additional or different permissions. However, no additional obligations are imposed on any author or copyright holder as a result of your choosing to follow a later version.

后续许可证版本可能会授予您额外的或不同的权限。但是，任何作者或版权所有者不会因您选择遵循后续版本而被施加任何额外的义务。

**15. Disclaimer of Warranty.15.无保证免责声明。**

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

在所适用法律所允许的范围内，本程序没有任何保证。除非另有书面声明，版权所有者和/或其他当事方系按“原样”提供本程序，没有任何明示或暗示的保证，包括但不限于有关适销性和适合特定目的的默示保证。关于本程序的质量和性能的全部风险都由您来承担。如果本程序确有缺陷，您自行承担所有必要的维护、维修或更正的费用。

**16. Limitation of Liability.16.责任限制。**

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

除非所适用法律要求或经书面同意，在任何情况下，任何版权所有者或任何根据前述条款修改和/或传递本程序的其他当事方，均不对您承担损害赔偿责任，包括因使用或无法使用本程序而产生的任何一般、特殊、附带或间接损害（包括但不限于数据丢失或数据不准确造成的损失、或本程序与其他程序运行失败造成您或第三方遭受的损失），即使该所有者或其他当事方已被告知此类损害的可能性。

**17. Interpretation of Sections 15 and 16.  17.对第15条和第16条的解释。**

If the disclaimer of warranty and limitation of liability provided above cannot be given local legal effect according to their terms, reviewing courts shall apply local law that most closely approximates an absolute waiver of all civil liability in connection with the Program, unless a warranty or assumption of liability accompanies a copy of the Program in return for a fee.

如果前述无保证免责声明和责任限制因其条款无法被赋予当地法律效力，则审查法院应适用与绝对放弃本程序相关所有民事责任最为接近的当地法律，除非本程序副本随附有收费提供的保证或担责承诺。

END OF TERMS AND CONDITIONS条款和条件结束

**How to Apply These Terms to Your New Programs**

如何将这些条款应用于您的新项目

If you develop a new program, and you want it to be of the greatest possible use to the public, the best way to achieve this is to make it free software which everyone can redistribute and change under these terms.

如果您开发了一个新的程序，并希望让它可以被公众进行最广泛应用，则最佳实现方式是让它成为自由软件，每个人都可以在这些条款下进行重新分发和修改。

To do so, attach the following notices to the program. It is safest to attach them to the start of each source file to most effectively state the exclusion of warranty; and each file should have at least the “copyright” line and a pointer to where the full notice is found.

为此，请在该程序中附加以下声明。最安全的做法是将它们添加到每个源文件的开头以最有效地声明排除保证；每个文件都至少应该有“版权”行和指向完整声明的提示。


```
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

<用一行给出此程序的名称和对其用途的概述。>
版权所有(C) <年>  <作者的名字>
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
此程序是自由软件；您可以对其进行重新发布和/或修改，
遵循自由软件基金会发布的GNU Affero通用公共许可证的条款；
根据该许可证第3版，或
（根据您的选择）任何后续版本。


This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
分发本程序是希望它能有用，但没有任何保证；
甚至没有对适销性或适合于特定目的的默示保证。 更多细节
请参阅GNU Affero通用公共许可证。

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

您应该已收到一份随附于本程序的GNU Affero通用公共许可证的副本；
如果没有，请见<http://www.gnu.org/licenses/>
并添加关于如何通过电子和纸质邮件联系您的信息。

```

If your software can interact with users remotely through a computer network, you should also make sure that it provides a way for users to get its source. For example, if your program is a web application, its interface could display a "Source" link that leads users to an archive of the code. There are many ways you could offer source, and different solutions will be better for different programs; see section 13 for the specific requirements.

如果您的程序可以与用户通过计算机网络进行远程交互，您应当保证其向用户提供了获取其源代码的方式。例如，如果您的程序是一个网页应用，其交互界面可以展示一个引导用户访问代码存档的“源代码”链接。您可以通过多种方式提供源代码，不同的程序会有不同的更优方案；请参看第13条的特殊要求。

You should also get your employer (if you work as a programmer) or school, if any, to sign a “copyright disclaimer” for the program, if necessary. For more information on this, and how to apply and follow the GNU AGPL, see <http://www.gnu.org/licenses/>.

您还应该让您的雇主（如果您的工作是程序员）或学校，如有，在必要时就本程序签署一份“版权免责声明”。 要了解更多相关信息，及如何适用和履行GNU AGPL，请见<http://www.gnu.org/licenses/>.

End license text.
许可证文本结束。

