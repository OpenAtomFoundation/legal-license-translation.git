译者注：本人承诺，该译文系本人原创翻译。本人同意，如本人译文被开放原子开源基金会开源许可证翻译项目选为终版译本，本人同意将译文以CC0协议贡献至公有领域。本许可证原文链接：https://solderpad.org/licenses/SHL-0.51/



###  **SOLDERPAD HARDWARE LICENSE version 0.51**

 ### **SOLDERPAD硬件许可证0.51版本** 



This license is based closely on the Apache License Version 2.0, but is not approved or endorsed by the Apache Foundation. A copy of the non-modified Apache License 2.0 can be found at http://www.apache.org/licenses/LICENSE-2.0.

本许可证紧密基于Apache许可证2.0版本，但未经Apache基金会的批准或认可。未经修改的Apache许可证2.0版本的副本可在 http://www.apache.org/licenses/LICENSE-2.0 获得。

As this license is not currently OSI or FSF approved, the Licensor permits any Work licensed under this License, at the option of the Licensee, to be treated as licensed under the Apache License Version 2.0 (which is so approved).

由于本许可证尚未经OSI或FSF批准，许可方允许被许可方根据自身选择，将根据本许可证许可的任何“作品”视为以Apache许可证2.0版本（经OSI和FSF批准）进行许可。


This License is licensed under the terms of this License and in particular clause 7 below (Disclaimer of Warranties) applies in relation to its use.

本许可是根据本许可证的条款，特别是下述第7条（免责声明）进行使用授权。


**TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION**

**使用、复制和分发的条款和条件**


**1.Definitions 定义**

"License" shall mean the terms and conditions for use, reproduction, and distribution as defined by Sections 1 through 9 of this document.

“本许可证”是指本文件第1至9条定义的使用、复制和分发的条款和条件。

"Licensor" shall mean the Rights owner or entity authorized by the Rights owner that is granting the License.

“许可方”是指授予许可的“权利”所有人或经“权利”所有人授权的实体。

"Legal Entity" shall mean the union of the acting entity and all other entities that control, are controlled by, or are under common control with that entity. For the purposes of this definition, "control" means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.

“法人实体”是指行为实体与控制该实体、被该实体控制或与该实体受共同控制的所有其他实体的集合。在本定义中，“控制”是指：(i) 通过合同或其他方式，直接或间接地引导或管理该实体的权力，或 (ii) 拥有百分之五十（50%）或以上流通股的所有权，或 (iii) 拥有该实体的实益所有权。

"You" (or "Your") shall mean an individual or Legal Entity exercising permissions granted by this License.

“您”（或“您的”）是指行使本许可证下许可的个人或法人实体。

"Rights" means copyright and any similar right including design right (whether registered or unregistered), semiconductor topography (mask) rights and database rights (but excluding Patents and Trademarks).

“权利”是指版权和任何类似权利，包括设计权（无论注册与否）、半导体拓扑图（掩膜）权和数据库权（但不包括专利和商标）。

"Source" form shall mean the preferred form for making modifications, including but not limited to source code, net lists, board layouts, CAD files, documentation source, and configuration files.

“源”形式是指进行修改的首选形式，包括但不限于源代码、网表、电路板布局、CAD文件、文档源和配置文件。

"Object" form shall mean any form resulting from mechanical transformation or translation of a Source form, including but not limited to compiled object code, generated documentation, the instantiation of a hardware design and conversions to other media types, including intermediate forms such as bytecodes, FPGA bitstreams, artwork and semiconductor topographies (mask works).

“目标”形式是指通过对“源”形式进行机械转换或转化而产生的任何形式，包括但不限于编译的目标代码、生成的文档、硬件设计的实例化以及向其他媒体类型的转换，包括字节码、FPGA比特流、原图和半导体拓扑图（掩膜作品）等中间形式。

"Work" shall mean the work of authorship, whether in Source form or other Object form, made available under the License, as indicated by a Rights notice that is included in or attached to the work (an example is provided in the Appendix below).

“本作品”是指根据作品中包含的或作品附有的“权利”声明（可见下文附录中举例）指向的以“本许可证”提供的原创作品，不论是“源”形式还是其他“目标”形式。

"Derivative Works" shall mean any work, whether in Source or Object form, that is based on (or derived from) the Work and for which the editorial revisions, annotations, elaborations, or other modifications represent, as a whole, an original work of authorship. For the purposes of this License, Derivative Works shall not include works that remain separable from, or merely link (or bind by name) or physically connect to or interoperate with the interfaces of, the Work and Derivative Works thereof.

“衍生作品”是指基于（或衍生于）“本作品”的任何作品，无论是“源”形式或“目标”形式，且对“本作品”的编辑修订、注释、阐述或其他修改在整体上形成原创作品。就“本许可证”而言，“衍生作品”不包括与“本作品”及其“衍生作品”仍保持分离，或仅通过接口连接（或以名称绑定），或通过物理连接，或与其接口可互操作的作品。

"Contribution" shall mean any design or work of authorship, including the original version of the Work and any modifications or additions to that Work or Derivative Works thereof, that is intentionally submitted to Licensor for inclusion in the Work by the Rights owner or by an individual or Legal Entity authorized to submit on behalf of the Rights owner. For the purposes of this definition, "submitted" means any form of electronic, verbal, or written communication sent to the Licensor or its representatives, including but not limited to communication on electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, the Licensor for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by the Rights owner as "Not a Contribution."

“贡献”是指由“权利”所有人或由经其授权的个人或“法人实体”有意提交给“许可方”以纳入“本作品”的任何原创作品或设计，包括“本作品”的原始版本和对“本作品”的任何修改或添加或其“衍生作品”。就本条定义而言，“提交”是指发送给“许可方”或其代表的任何电子、口头或书面形式的通信内容，包括但不限于为供讨论和改进“本作品”目的，发送到“许可方”管理的、或代表“许可方”的电子邮件列表、源代码控制系统和发布跟踪系统的通信内容，但不包括“权利”所有人明确标注或以其他方式书面指定为“非贡献”的通信内容。

"Contributor" shall mean Licensor and any individual or Legal Entity on behalf of whom a Contribution has been received by Licensor and subsequently incorporated within the Work.

“贡献者” 是指“许可方”，以及其“贡献”已被“许可方”接收且“许可方”代表其将“贡献”合并入“本作品”中的任何个人或“法人实体”。

 **2.Grant of License.**  Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable license under the Rights to reproduce, prepare Derivative Works of, publicly display, publicly perform, sublicense, and distribute the Work and such Derivative Works in Source or Object form and do anything in relation to the Work as if the Rights did not exist.

 **授权许可** 受限于“本许可证”的条款和条件，每个“贡献者”特此授予您永久的、全球性的、非排他性的、免交易费的、免许可费的、不可撤销的“权利”许可，以复制、准备“衍生作品”、公开展示、公开表演、分许可和分发“源”形式或“目标”形式的“本作品”及其“衍生作品”，以及对“本作品”进行任何操作，如同“权利”不存在。


**3.Grant of Patent License.**  Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work, where such license applies only to those patent claims licensable by such Contributor that are necessarily infringed by their Contribution(s) alone or by combination of their Contribution(s) with the Work to which such Contribution(s) was submitted. If You institute patent litigation against any entity (including a cross-claim or counterclaim in a lawsuit) alleging that the Work or a Contribution incorporated within the Work constitutes direct or contributory patent infringement, then any patent licenses granted to You under this License for that Work shall terminate as of the date such litigation is filed.

 **专利许可** 受限于“本许可证”的条款和条件，每个“贡献者”特此授予您永久的、全球性的、非排他性的、免交易费的、免许可费的、不可撤销的（按本节规定的除外）专利许可，以制造、委托制造、使用、许诺销售、销售、进口及以其他方式转移“本作品”，该许可仅及于“贡献者”有权许可且由其“贡献”本身或由其“贡献”与其提交时所针对的“本作品”形成的组合所必然侵犯的专利权利要求。如果您对任何实体提起专利诉讼（包括在法律诉讼中的交叉诉讼主张或反诉主张），主张“本作品”或纳入“本作品”的“贡献”构成专利的直接侵权或帮助侵权，则在“本许可证”下授予您的任何专利许可自该诉讼提起之日起终止。

 **4.Redistribution.**  You may reproduce and distribute copies of the Work or Derivative Works thereof in any medium, with or without modifications, and in Source or Object form, provided that You meet the following conditions:

 **再分发** 您可在任何媒介上复制和分发“本作品”或其“衍生作品”的副本，无论是否经过修改，无论以“源”形式或“目标”形式，只要您满足以下条件：


  （1）You must give any other recipients of the Work or Derivative Works a copy of this License; and
 
   您必须向“本作品”或“衍生作品”的任何其他接收者提供“本许可证”的副本；且
 
 （2）You must cause any modified files to carry prominent notices stating that You changed the files; and

  您必须使任何修改后的文件都附上显著的声明，说明您修改了该文件；且

 （3）You must retain, in the Source form of any Derivative Works that You distribute, all copyright, patent, trademark, and attribution notices from the Source form of the Work, excluding those notices that do not pertain to any part of the Derivative Works; and

您必须在您分发的任何“衍生作品”的“源”形式中，保留来自“本作品”“源”形式的所有版权、专利、商标和权属声明，但与该“衍生作品”任何部分无关的声明除外；以及

 （4）If the Work includes a "NOTICE" text file as part of its distribution, then any Derivative Works that You distribute must include a readable copy of the attribution notices contained within such NOTICE file, excluding those notices that do not pertain to any part of the Derivative Works, in at least one of the following places: within a NOTICE text file distributed as part of the Derivative Works; within the Source form or documentation, if provided along with the Derivative Works; or, within a display generated by the Derivative Works, if and wherever such third-party notices normally appear. The contents of the NOTICE file are for informational purposes only and do not modify the License. You may add Your own attribution notices within Derivative Works that You distribute, alongside or as an addendum to the NOTICE text from the Work, provided that such additional attribution notices cannot be construed as modifying the License. You may add Your own copyright statement to Your modifications and may provide additional or different license terms and conditions for use, reproduction, or distribution of Your modifications, or for any such Derivative Works as a whole, provided Your use, reproduction, and distribution of the Work otherwise complies with the conditions stated in this License.

  如果“本作品”分发时包含“NOTICE”文本文件，则您分发的任何“衍生作品”必须至少在下述一处位置包含本文件中的署名声明的可读副本，但与该“衍生作品”任何部分无关的声明除外：在作为“衍生作品”一部分的NOTICE文本文件中；在与“衍生作品”一并提供的“源”形式或文档中；或在“衍生作品”生成的显示界面中通常展示第三方声明的任何位置（如有）。NOTICE文件中的内容仅为提供信息，并不修改“本许可证”。您可以在您分发的“衍生作品”中添加您自己的权属声明，与“本作品”的NOTICE文本并列或作为其附录，只要该添加的权属声明不被解释为修改“本许可证”。您可以对您修改的部分添加您自己的版权声明，并可以就您修改的部分或任何“衍生作品”整体提供额外或不同的许可条款和条件以供使用、复制或分发，只要您使用、复制和分发“本作品”的做法符合“本许可证”规定的条件。


 **5.Submission of Contributions.** Unless You explicitly state otherwise, any Contribution intentionally submitted for inclusion in the Work by You to the Licensor shall be under the terms and conditions of this License, without any additional terms or conditions. Notwithstanding the above, nothing herein shall supersede or modify the terms of any separate license agreement you may have executed with Licensor regarding such Contributions.

 **贡献的提交**  除非另有明确说明，您有意向“许可方”提交且为了纳入“本作品”的任何“贡献”，均受“本许可证”的条款和条件约束，而不带有任何附加条款或条件。尽管有上述规定，“本许可证”中任何内容，均不会取代或修改您就该“贡献”可能与“许可方”另行签署的许可协议的条款。


 **6.Trademarks.**  This License does not grant permission to use the trade names, trademarks, service marks, or product names of the Licensor, except as required for reasonable and customary use in describing the origin of the Work and reproducing the content of the NOTICE file.

 **商标**  “本许可证”不授予使用“许可方”的商号、商标、服务标志或产品名称的许可，但在描述“本作品”来源及复制NOTICE文件内容时因合理和习惯用法需要的除外。

 **7.Disclaimer of Warranty.** Unless required by applicable law or agreed to in writing, Licensor provides the Work (and each Contributor provides its Contributions) on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, including, without limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE. You are solely responsible for determining the appropriateness of using or redistributing the Work and assume any risks associated with Your exercise of permissions under this License.

 **免责声明**  除非所适用法律要求或经书面同意，“许可方”就“本作品”（及每个“贡献者”就其“贡献”）是 **“按原样” 提供，没有任何明示或默示的保证或条件，**包括但不限于关于**权属、不侵权、适销性或特定目的适用性**的任何保证或条件。您自行负责确认使用或再分发“本作品”的适当性，并承担与您行使“本许可证”下所授许可相关的任何风险。

 **8.Limitation of Liability.**  In no event and under no legal theory, whether in tort (including negligence), contract, or otherwise, unless required by applicable law (such as deliberate and grossly negligent acts) or agreed to in writing, shall any Contributor be liable to You for damages, including any direct, indirect, special, incidental, or consequential damages of any character arising as a result of this License or out of the use or inability to use the Work (including but not limited to damages for loss of goodwill, work stoppage, computer failure or malfunction, or any and all other commercial damages or losses), even if such Contributor has been advised of the possibility of such damages.

 **责任限制**  在任何情况和任何法律理论下，无论在侵权（包括过失）、合同或其他方面，除非所适用法律要求（如故意或严重过失行为）或经书面同意，任何“贡献者”均不对您的损失承担责任，包括因“本许可证”或因使用或无法使用“本作品”而导致的任何直接的、间接的、特定的、偶然性的或继发性的损失（包括但不限于商誉损失、停工、计算机失灵或故障，或任何其他商业损害或损失），即使该“贡献者”曾被告知该损失的可能性。

 **9.Accepting Warranty or Additional Liability.**  While redistributing the Work or Derivative Works thereof, You may choose to offer, and charge a fee for, acceptance of support, warranty, indemnity, or other liability obligations and/or rights consistent with this License. However, in accepting such obligations, You may act only on Your own behalf and on Your sole responsibility, not on behalf of any other Contributor, and only if You agree to indemnify, defend, and hold each Contributor harmless for any liability incurred by, or claims asserted against, such Contributor by reason of your accepting any such warranty or additional liability.

 **接受保证或附加责任**  在再分发“本作品”或其“衍生作品”时，您可以选择接受支持、保证、补偿或其他符合“本许可证”的责任义务和/或权利，并为之收取费用。然而，在接受该义务时，您只能代表您自己行事且责任完全由您承担，不得代表任何其他“贡献者”，且在其他“贡献者”因您接受此类保证或附加责任而产生赔偿责任或主张时，您必须同意补偿、辩护并使该“贡献者”免受该损害。

 **END OF TERMS AND CONDITIONS 条件和条款结束** 



 **APPENDIX: How to apply this license to your work 附录：如何将本许可证应用到您的作品中** 

To apply this license to your work, attach the following boilerplate notice, with the fields enclosed by brackets "[]" replaced with your own identifying information. (Don't include the brackets!) The text should be enclosed in the appropriate comment syntax for the file format. We also recommend that a file or class name and description of purpose be included on the same "printed page" as the copyright notice for easier identification within third-party archives.

要将本许可证应用于您的作品，请附上下述声明模板，并将由括号“[]”包围的字段替换为您自己的标识信息。（不要包含方括号！）该文本应纳入文件格式适合的注释语法中。我们还建议将文件名或类名和目的描述与版权声明放在同一“打印页面”上，以便于在第三方档案中识别。

Copyright [yyyy] [name of copyright owner] Copyright and related rights are licensed under the Solderpad Hardware License, Version 0.51 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
http://solderpad.org/licenses/SHL-0.51.
Unless required by applicable law or agreed to in writing, software, hardware and materials distributed under this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

版权所有[yyyy][版权所有者名称]版权和相关权利根据Solderpad硬件许可证第0.51版（“本许可证”）许可；除非符合本许可证的规定，否则您不得使用此文件。您可以在此获取本许可证的副本：
http://solderpad.org/licenses/SHL-0.51
除非所适用法律要求或经书面同意，在本许可证下分发的软件、硬件和材料是“按原样”分发的，没有任何形式的保证或条件，不论明示或默示。请查阅本许可证了解有关本许可证下许可和限制的具体要求。