**译者注：本人Vanessa（vanessa-guo@outlook.com）承诺，该译文系本人原创翻译。本人同意，如本人译文被源译识开源许可证翻译板块选为终版译本，本人同意将译文以CC0协议贡献至公有领域。本许可证原文链接：https://tapr.org/the-tapr-open-hardware-license/ 。本许可证由John Ackermann律师起草，Bruce Perens和Eric S. Raymond审核，并发布在TAPR社区中。本许可证明确，硬件开源更依赖专利许可，而非版权许可。TAPR全称是Tucson Amateur Packet Radio, 是为无线电爱好者提供领导力和资源的社区，旨在推动无线电艺术的发展。*



## The TAPR Open Hardware License
## Version 1.0 (May 25, 2007)

Copyright 2007 TAPR – http://www.tapr.org/OHL

## TAPR开源硬件许可证
## 第1.0版 （2007年5月25日）

版权所有 2007 TAPR - http://www.tapr.org/OHL

#### PREAMBLE 序言

Open Hardware is a thing – a physical artifact, either electrical or mechanical – whose design information is available to, and usable by, the public in a way that allows anyone to make, modify, distribute, and use that thing.  In this preface, design information is called “documentation” and things created from it are called “products.”

开源硬件是一种物品——电子或机械的有形物——其设计信息可供公众访问并以允许任何人制造、修改、分发和使用该物品的方式对其使用。在本序言中，设计信息被称为“文档”，根据文档创作出的物品被称为“产品”。

The TAPR Open Hardware License (“OHL”) agreement provides a legal framework for Open Hardware projects.  It may be used for any kind of product, be it a hammer or a computer motherboard, and is TAPR's contribution to the community; anyone may use the OHL for their Open Hardware project.  You are free to copy and use this document provided only that you do not change it.

TAPR开源硬件许可证（“OHL”）协议为开源硬件项目提供了一个法律框架。它可以在各类产品上使用，无论是锤子还是电脑主版，均作为TAPR对于社区的贡献；任何人可以在他们的开源硬件项目中使用OHL。您可以自由地复制和使用本文件，只要且仅要您不修改它。

Like the GNU General Public License, the OHL is designed to guarantee your freedom to share and to create.  It forbids anyone who receives rights under the OHL to deny any other licensee those same rights to copy, modify, and distribute documentation, and to make, use and distribute products based on that documentation.

OHL与GNU通用公共许可证一样，旨在确保您分享和创作的自由。它禁止，任何获得OHL下授予的权利的人否认任何其他被许可方的复制、修改和分发“文档”的相同权利，以及制作、使用和分发基于该“文档”的“产品”的相同权利。

Unlike the GPL, the OHL is not primarily a copyright license.  While copyright protects documentation from unauthorized copying, modification, and distribution, it has little to do with your right to make, distribute, or use a product based on that documentation.  For better or worse, patents play a significant role in those activities.  Although it does not prohibit anyone from patenting inventions embodied in an Open Hardware design, and of course cannot prevent a third party from enforcing their patent rights, those who benefit from an OHL design may not bring lawsuits claiming that design infringes their patents or other intellectual property.

不像GPL，OHL并非主要是个版权许可。虽然版权保护“文档”免于未经授权的复制、修改和分发，但它对您基于该“文档”制作、分发或使用“产品”的权利没有什么帮助。或好或坏，专利在这些活动中都扮演着重要角色。虽然它并不禁止任何人为开源硬件设计中蕴含的发明申请专利，当然也无法阻止第三方行使其专利权，但那些受益于OHL设计的人不得提起诉讼去主张该设计侵犯了他们的专利或其他知识产权。

The OHL addresses unique issues involved in the creation of tangible, physical things, but does not cover software, firmware, or code loaded into programmable devices.  A copyright-oriented license such as the GPL better suits these creations.

OHL关注的是有形实物在创作中涉及的特有问题，而不关注软件、固件或嵌入到可编程设备中的代码。GPL等以版权为导向的许可证则更适合后者的创作。

How can you use the OHL, or a design based upon it?  While the terms and conditions below take precedence over this preamble, here is a summary:

您如何使用OHL或者基于OHL的设计？由于下述的条款与条件比序言优先适用，概述如下：

- You may modify the documentation and make products based upon it.
- 您可以修改“文档”并基于它来制造“产品”。
- You may use products for any legal purpose without limitation.
- 您可以将“产品”用于任何合法目的，而不受限制。
- You may distribute unmodified documentation, but you must include the complete package as you received it.
- 您可以分发未经修改的“文档”，但必须包括您收到的完整文档包。
- You may distribute products you make to third parties, if you either include the documentation on which the product is based, or make it available without charge for at least three years to anyone who requests it.
- 您可以向第三方分发您制造的“产品”，但您必须提供“产品”所依据的“文档”，或在至少三年内免费提供给提出请求的任何人。
- You may distribute modified documentation or products based on it, if you:
- 您可以分发修改后的“文档”或基于它的“产品”，条件是：
```
○ License your modifications under the OHL.
○ Include those modifications, following the requirements stated below.  
○ Attempt to send the modified documentation by email to any of the developers who have provided their email address.  This is a good faith obligation – if the email fails, you need do nothing more and may go on with your distribution.
○ 按照OHL将您的修改许可。
○ 遵循以下要求，并涵盖这些修改。
○ 尝试将修改后的“文档”通过电子邮件发送给任何提供了电子邮件地址的开发人员。这是一项善意义务——如果电子邮件发送失败，您无需采取进一步行动，可以继续您的分发行为。
```
- If you create a design that you want to license under the OHL, you should:
- 如果您创作了一份设计且您希望根据OHL许可您的设计，您应该：
```
○ Include this document in a file named LICENSE (with the appropriate extension) that is included in the documentation package.
○ If the file format allows, include a notice like “Licensed under the TAPR Open Hardware License (www.tapr.org/OHL)” in each documentation file.  While not required, you should also include this notice on printed circuit board artwork and the product itself; if space is limited the notice can be shortened or abbreviated.
○ Include a copyright notice in each file and on printed circuit board artwork.
○ If you wish to be notified of modifications that others may make, include your email address in a file named “CONTRIB.TXT” or something similar.
○ 在“文档”包中提供的一个名为LICENSE（恰当的扩展名）的文件中，包含本文件。
○ 如果文件格式允许，请在每个“文档”文件中注明“根据TAPR开源硬件许可协议(www.tapr.org/OHL) 许可”。虽然并非必须的，但也应在印刷电路板图样和“产品”本身中包含该声明；如果空间有限，声明可以缩短或缩写。
○ 在每个文件和印制电路板图样中包含版权声明。
○ 如果您希望收到他人的修改通知，请在名为“CONTRIB.TXT”或类似文件中包含您的电子邮件地址。
```
- Any time the OHL requires you to make documentation available to others, you must include all the materials you received from the upstream licensors.  In addition, if you have modified the documentation:
- 任何时候当OHL要求您向他人提供“文档”时，您必须包含从上游许可方获得的所有材料。此外，如果您修改了“文档”：
```
○ You must identify the modifications in a text file (preferably named “CHANGES.TXT”) that you include with the documentation.  That file must also include a statement like “These modifications are licensed under the TAPR Open Hardware License.”
○ You must include any new files you created, including any manufacturing files (such as Gerber files) you create in the course of making products.
○ You must include both “before” and “after” versions of all files you modified.
○ You may include files in proprietary formats, but you must also include open format versions (such as Gerber, ASCII, Postscript, or PDF) if your tools can create them.
○ 您必须在一个文本文件（最好命名为“CHANGES.TXT”）中标明修改内容，并将其与“文档”一起提供。该文件还必须包含类似“这些修改在TAPR开源硬件许可协议下许可”的声明。
○ 您必须包含您创建的任何新文件，包括您在制造“产品”过程中创建的任何制作性文件（如Gerber文件）。
○ 您必须包含所有您修改的文件的“修改前”版本和“修改后”版本。
○ 您可以包含专有格式的文件，但如果您的工具可以创建开放格式的文件（如Gerber、ASCII、Postscript或PDF），则您也必须包含这些文件。
```

#### TERMS AND CONDITIONS 条款与条件

**1.Introduction 简介**

1.1	This Agreement governs how you may use, copy, modify, and distribute Documentation, and how you may make, have made, and distribute Products based on that Documentation.  As used in this Agreement, to “distribute” Documentation means to directly or indirectly make copies available to a third party, and to “distribute” Products means to directly or indirectly give, loan, sell or otherwise transfer them to a third party.

1.1	本协议规定了您如何使用、复制、修改和分发“文档”，以及如何制造、委托制造和分发基于该“文档”的“产品”。在本协议中，“文档”的“分发”是指直接或间接地向第三方提供副本，“产品”的“分发”是指直接或间接地向第三方提供、出借、出售或以其他方式转移产品。

1.2	“Documentation” includes:
```
(a) schematic diagrams;
(b) circuit or circuit board layouts, including Gerber and other data files used for manufacture;
(c) mechanical drawings, including CAD, CAM, and other data files used for manufacture;
(d) flow charts and descriptive text; and
(e) other explanatory material.
```
Documentation may be in any tangible or intangible form of expression, including but not limited to computer files in open or proprietary formats and representations on paper, film, or other media.

1.2	“文档”包括：
```
(a) 示意图；
(b) 电路或电路板布局，包括用于制作的Gerber和其他数据文件；
(c) 机械图纸，包括CAD、CAM和其他用于制造的数据文件；
(d) 流程图和说明性文字；以及
(e) 其他解释性材料。
```
“文档”可以是任何有形或无形的表达形式，包括但不限于开放或专有格式下的计算机文件以及纸面、影片或其他媒体上的表达。

1.3	“Products” include:
```
(a) circuit boards, mechanical assemblies, and other physical parts and components;
(b) assembled or partially assembled units (including components and subassemblies); and
(c) parts and components combined into kits intended for assembly by others;
```
which are based in whole or in part on the Documentation.

1.3	“产品”包括全部或部分基于“文档”的：
```
(a) 电路板、机械组件和其他有形零部件；
(b) 已组装或部分组装的单元（包括部件和组件）；以及
(c) 组合成套件供他人组装的零部件；
```

1.4	This Agreement applies to any Documentation which contains a notice stating it is subject to the TAPR Open Hardware License, and to all Products based in whole or in part on that Documentation.  If Documentation is distributed in an archive (such as a “zip” file) which includes this document, all files in that archive are subject to this Agreement unless they are specifically excluded.  Each person who contributes content to the Documentation is referred to in this Agreement as a “Licensor.”

1.4	本协议适用于任何包含受TAPR开源硬件许可证约束的声明的“文档”，以及全部或部分基于该“文档”的所有“产品”。如果“文档”是以包含本文本的压缩包（如“zip”文件）形式发布，则该压缩包中的所有文件均适用本协议，除非这些文件被明确排除适用。在本协议中，向文档中贡献内容的每个人都被称为“许可方”。

1.5	By (a) using, copying, modifying, or distributing the Documentation, or (b) making or having Products made or distributing them, you accept this Agreement, agree to comply with its terms, and become a “Licensee.”  Any activity inconsistent with this Agreement will automatically terminate your rights under it (including the immunities from suit granted in Section 2), but the rights of others who have received Documentation, or have obtained Products, directly or indirectly from you will not be affected so long as they fully comply with it themselves.

1.5	通过 (a) 使用、复制、修改或分发“文档”，或 (b) 制造或委托制造或分发“产品”，您接受本协议，并同意遵守其条款，并成为“被许可方”。任何与本协议不一致的活动都将自动终止您在本协议项下的权利（包括第2条中授予的诉讼豁免权），但直接或间接从您处获得“文档”或“产品”的其他人的权利不受影响，只要他们自己完全遵守本协议即可。

1.6	This Agreement does not apply to software, firmware, or code loaded into programmable devices which may be used in conjunction with Documentation or Products.  Such software is subject to the license terms established by its copyright holder(s).

1.6	本协议不适用于嵌入到可编程设备的软件、固件或代码，它们可能与“文档”或“产品”一起使用。此类软件应遵守其版权所有者制定的许可条款。

**2.Patents 专利权**

2.1	Each Licensor grants you, every other Licensee, and every possessor or user of Products a perpetual, worldwide, and royalty-free immunity from suit under any patent, patent application, or other intellectual property right which he or she controls, to the extent necessary to make, have made, possess, use, and distribute Products.  This immunity does not extend to infringement arising from modifications subsequently made by others.

2.1	每个许可方向您、每个其他被许可方及每个“产品”的持有者或使用者，授予一个永久的、全球性的和免授权费的豁免权，使其在制造、委托制造、持有、使用和分发“产品”所必需的范围内，免受许可方控制的任何专利、专利申请或其他知识产权的起诉。该豁免权不延伸至因他人随后进行的修改而引发的侵权行为。

2.2	If you make or have Products made, or distribute Documentation that you have modified, you grant every Licensor, every other Licensee, and every possessor or user of Products a perpetual, worldwide, and royalty-free immunity from suit under any patent, patent application, or other intellectual property right which you control, to the extent necessary to make, have made, possess, use, and distribute Products.  This immunity does not extend to infringement arising from modifications subsequently made by others.

2.2	如果您制造或委托制造“产品”，或分发经您修改过的“文档”，则您向每个许可方、每个其他被许可方以及每个“产品”的持有者或使用者，授予一个永久的、全球性的和免授权费的豁免权，使其在制造、委托制造、持有、使用和分发“产品”所必需的范围内，免受您控制的任何专利、专利申请或其他知识产权的起诉。该豁免权不延伸至因他人随后进行的修改而引发的侵权行为。

2.3	To avoid doubt, providing Documentation to a third party for the sole purpose of having that party make Products on your behalf is not considered “distribution,” and a third party's act of making Products solely on your behalf does not cause that party to grant the immunity described in the preceding paragraph.

2.3	为免疑义，向第三方提供“文档”的唯一目的是让该方代表您制造“产品”，这不被视为“分发”，第三方仅代表您制造“产品”的行为不会导致该方授予上段所述的豁免权。

2.4	These grants of immunity are a material part of this Agreement, and form a portion of the consideration given by each party to the other.  If any court judgment or legal agreement prevents you from granting the immunity required by this Section, your rights under this Agreement will terminate and you may no longer use, copy, modify or distribute the Documentation, or make, have made, or distribute Products.

2.4	授予这些豁免权是本协议的重要组成部分，是每一方给予另一方的对价的一部分。如果任何法院判决书或法律协议阻止您授予本节所要求的豁免权，您在本协议项下的权利将终止，您不得再使用、复制、修改或分发“文档”，也不得制造、委托制造或分发“产品”。

**3.Modifications 修改**

You may modify the Documentation, and those modifications will become part of the Documentation.  They are subject to this Agreement, as are Products based in whole or in part on them.  If you distribute the modified Documentation, or Products based in whole or in part upon it, you must email the modified Documentation in a form compliant with Section 4 to each Licensor who has provided an email address with the Documentation.  Attempting to send the email completes your obligations under this Section and you need take no further action if any address fails.

您可以修改“文档”，这些修改将成为“文档”的一部分。它们受本协议约束，全部或部分基于它们的“产品”也受本协议约束。如果您分发修改后的“文档”或全部或部分基于该“文档”的“产品”，您必须将修改后的“文档”以符合第4条规定的形式通过电子邮件发送给为“文档”提供了电子邮件地址的每位许可方。尝试发送电子邮件即完成本节规定的义务，如果任何地址发送失败，您无需采取进一步行动。

**4.Distributing Documentation 分发“文档”**

4.1	You may distribute unmodified copies of the Documentation in its entirety in any medium, provided that you retain all copyright and other notices (including references to this Agreement) included by each Licensor, and include an unaltered copy of this Agreement.

4.1	您可以在任何媒体上分发未经修改的完整“文档”副本，但您必须保留每个许可方包含的所有版权和其他声明（包括对本协议的引述），并包含一份未经修改的本协议副本。

4.2	You may distribute modified copies of the Documentation if you comply with all the requirements of the preceding paragraph and:
```
(a) include a prominent notice in an ASCII or other open format file identifying those elements of the Documentation that you changed, and stating that the modifications are licensed under the terms of this Agreement;
(b) include all new documentation files that you create, as well as both the original and modified versions of each file you change (files may be in your development tool's native file format, but if reasonably possible, you must also include open format, such as Gerber, ASCII, Postscript, or PDF, versions);
(c) do not change the terms of this Agreement with respect to subsequent licensees; and
(d) if you make or have Products made, include in the Documentation all elements reasonably required to permit others to make Products, including Gerber, CAD/CAM and other files used for manufacture.
```

4.2	如果您遵守上段的所有要求，并符合以下条件，您可以分发修改后的“文档”的副本：
```
(a) 在一个ASCII或其他开放格式文件中包含一个醒目声明，说明您所更改的“文档”的要素，并说明这些修改是在本协议的条款下许可的；
(b) 包括您创建的所有新文档文件，以及您更改的每个文件的原始版本和修改版本（文件可以是开发工具的本地文件格式，但如果可能，您还必须包括开放格式，如Gerber、ASCII、Postscript或PDF版本）；
(c) 不改变本协议有关后继被许可方的条款；以及
(d) 如果您制造或委托制造“产品”，则应在“文档”中包含允许他人制造“产品”所合理需要的所有要素，包括用于制作的Gerber、CAD/CAM和其他文件。
```

**5.Making Products 制造“产品”**

5.1	You may use the Documentation to make or have Products made, provided that each Product retains any notices included by the Licensor (including, but not limited to, copyright notices on circuit boards).

5.1	您可以使用“文档”制造或委托制造“产品”，但每件“产品”必须保留许可方的任何声明（包括但不限于电路板上的版权声明）。

5.2	You may distribute Products you make or have made, provided that you include with each unit a copy of the Documentation in a form consistent with Section 4.  Alternatively, you may include either (i) an offer valid for at least three years to provide that Documentation, at no charge other than the reasonable cost of media and postage, to any person who requests it; or (ii) a URL where that Documentation may be downloaded, available for at least three years after you last distribute the Product.

5.2	您可以分发您制造或委托制造的“产品”，但您必须以符合第4条规定的形式在每个部件中包含一份“文档”的副本。或者，您可以包括：(i) 有效期至少三年的要约，向任何提出要求的人免费提供该“文档”，但合理的媒体及邮寄成本是不免费的；或 (ii) 可以下载该文档的URL，该URL在您最后一次分发“产品”后至少三年内可用。

**6.NEW LICENSE VERSIONS 许可证的新版本**

TAPR may publish updated versions of the OHL which retain the same general provisions as the present version, but differ in detail to address new problems or concerns, and carry a distinguishing version number.  If the Documentation specifies a version number which applies to it and “any later version”, you may choose either that version or any later version published by TAPR.  If the Documentation does not specify a version number, you may choose any version ever published by TAPR.  TAPR owns the copyright to the OHL, but grants permission to any person to copy, distribute, and use it in unmodified form.

TAPR可能会发布开源硬件许可证的最新版本，这些版本保留了与当前版本相同的一般规定，但在细节上有所不同，旨在解决新的问题或关切，并带有不同的版本号。如果“文档”指定了适用于该版本和“任何后续版本”的版本号，则您可以选择该版本或由TAPR发布的任何后续版本。如果“文档”没有指定版本号，则您可以选择TAPR曾发布的任何版本。TAPR拥有OHL的版权，但允许任何人以未经修改的形式复制、分发和使用OHL。

**7.WARRANTY AND LIABILITY LIMITATIONS 担保及责任限制**

7.1	THE DOCUMENTATION IS PROVIDED ON AN “AS-IS” BASIS WITHOUT WARRANTY OF ANY KIND, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND TITLE, ARE HEREBY EXPRESSLY DISCLAIMED.

7.1	在适用法律允许的范围内，“文档”按“原样”提供，而不提供任何形式的保证。所有明示或默示的保证，包括但不限于任何适销性、特定用途适用性和所有权的保证，在此明确免责。

7.2	IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW WILL ANY LICENSOR BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, PUNITIVE, OR EXEMPLARY DAMAGES ARISING OUT OF THE USE OF, OR INABILITY TO USE, THE DOCUMENTATION OR PRODUCTS, INCLUDING BUT NOT LIMITED TO CLAIMS OF INTELLECTUAL PROPERTY INFRINGEMENT OR LOSS OF DATA, EVEN IF THAT PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

7.2	除非适用法律另有规定，否则任何许可方在任何情况下均不对您或任何第三方因使用或无法使用“文档”或“产品”而引发的任何直接的、间接的、偶然的、继发性的、惩罚性的或惩戒性的损失（包括但不限于知识产权侵权主张或数据丢失）承担责任，即使该方已被告知此类损害的可能性。

7.3	You agree that the foregoing limitations are reasonable due to the non-financial nature of the transaction represented by this Agreement, and acknowledge that were it not for these limitations, the Licensor(s) would not be willing to make the Documentation available to you.

7.3	您同意，鉴于本协议所呈现的交易并非是资金性质的，上述限制是合理的，并认可，如果没有这些限制，许可方不会愿意向您提供“文档”。

7.4	You agree to defend, indemnify, and hold each Licensor harmless from any claim brought by a third party alleging any defect in the design, manufacture, or operation of any Product which you make, have made, or distribute pursuant to this Agreement.

7.4	如果第三方指控您根据本协议制造、委托制造或分发的任何“产品”在设计、制作或操作方面存在任何缺陷，您同意为许可方辩护、对许可方进行赔偿并使许可方免受损失。